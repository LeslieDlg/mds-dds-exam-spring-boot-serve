FROM maven:latest AS maven
COPY src /home/app/src
RUN mvn install -DskipTests

FROM openjdk:18-jdk-alpine3.15
VOLUME /tmp
EXPOSE 8080
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
